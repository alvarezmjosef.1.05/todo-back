import express from "express";
import MongoClient from "mongodb";

const app = express();
const port = "3001";

MongoClient.connect("mongodb://localhost:27017/todoDB", { useUnifiedTopology: true })
.then(client => {
    console.log("conectado");
    const db = client.db("todoDB");

    app.use(express.json())

    app.use(function (req, res, next) {

        // Website you wish to allow to connect
        res.setHeader('Access-Control-Allow-Origin', '*');
    
        // Request methods you wish to allow
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    
        // Request headers you wish to allow
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    
        // Set to true if you need the website to include cookies in the requests sent
        // to the API (e.g. in case you use sessions)
        res.setHeader('Access-Control-Allow-Credentials', true);
    
        // Pass to next layer of middleware
        next();
    });

    app.get("/tasks", (req, res) =>  {
        db.collection("tasks").find().toArray()
        .then(response => {
            res.send(response);
        });
    })
    
    app.post("/tasks", (req, res) => {
        db.collection("tasks").insertOne({title: req.body.title, description: req.body.description, done: false});
        res.send("tarea agregada");
    })
    
    app.put("/tasks", (req, res) => {
        console.log(req.body);
        const oldTask = db.collection("tasks").updateOne(
        {
            _id: MongoClient.ObjectId(req.body.id)
        },
        {
            $set: { 
                title: req.body.newTitle, 
                description: req.body.newDescription,
                done: req.body.newDone
            }
        }
        
        )
        res.send({
            message: "tarea modificada",
            oldTask: {
                _id: req.body.id,
                title: req.body.newTitle,
                description: req.body.newDescription,
                done: req.body.newDone,
            }
        })
    })
     
    app.delete("/tasks", (req, res) => {
        console.log(req.body);
        db.collection("tasks")
        .findOneAndDelete({_id: MongoClient.ObjectId(req.body.id)})
        .then(response => res.send({
            message: "tarea eliminada",
            data: response
        }));

    })
    
    app.listen(port, () => {
        console.log("App in port " + port);
    })
})
.catch(error => console.error(error))
